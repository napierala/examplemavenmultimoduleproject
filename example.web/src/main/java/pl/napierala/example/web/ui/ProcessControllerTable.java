package pl.napierala.example.web.ui;

import com.vaadin.data.Container;
import com.vaadin.ui.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The ProcessController Table.
 */
public class ProcessControllerTable extends Table {

    static final Logger logger = LoggerFactory.getLogger(ProcessControllerTable.class);

    public ProcessControllerTable(Container container) {
        setContainerDataSource(container);

        setHeight("450px");
        setWidth("650px");
        configColumns();
    }

    /**
     * Config the columns of the table
     */
    private void configColumns() {
        setVisibleColumns(new Object[]{"id", "processControllerType"});
        setColumnHeaders(new String[]{"#", "Type"});

    }

}
