package pl.napierala.example.web.ui;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.dialogs.ConfirmDialog;
import pl.napierala.example.web.ds.ProcessControllerJPAContainer;
import pl.napierala.example.datasource.model.ProcessController;
import pl.napierala.example.datasource.model.ProcessControllerType;


/**
 * The window to work on the process controller(edit and create).
 */
public class ProcessControllerWindow extends Window implements Button.ClickListener {

    static final Logger logger = LoggerFactory.getLogger(ProcessControllerWindow.class);

    private FormLayout layout;

    /**
     * A binder to bind fields in the form to specific field in the model.
     */
    private BeanFieldGroup<ProcessController> binder;

    private HorizontalLayout buttons;
    private Button saveButton;
    private Button cancelButton;
    private Button deleteButton;

    /**
     * The Aircraft JPAContainer
     */
    private ProcessControllerJPAContainer datasource;

    public ProcessControllerWindow(ProcessControllerJPAContainer datasource) {
        this.datasource = datasource;
        init();
        setModal(true);
    }

    /**
     * Initialize this window.
     */
    private void init() {
        layout = new FormLayout();
        layout.setSizeFull();
        layout.setSpacing(true);

        saveButton = new Button("Save");
        saveButton.addClickListener(this);
        saveButton.setClickShortcut(KeyCode.ENTER);

        cancelButton = new Button("Cancel");
        cancelButton.addClickListener(this);
        cancelButton.setClickShortcut(KeyCode.ESCAPE);

        deleteButton = new Button("Delete");
        deleteButton.addClickListener(this);
        deleteButton.setVisible(false);

        buttons = new HorizontalLayout();
        buttons.addComponent(saveButton);
        buttons.addComponent(cancelButton);
        buttons.addComponent(deleteButton);

        setContent(layout);

        setHeight("370");
        setWidth("400");
    }

    /**
     * When this method is called, a edit window will be created for editing the processController.
     *
     * @param id
     *          The id of the processController to edit.
     */
    public void edit(Long id) {
        try {

            setCaption("Edit ProcessController");

            //Get the processController that will be edited from the datasource
            ProcessController m = datasource.getItem(id).getEntity();

            //Bind all of the fields that will be edited to the form's elements.
            bindingFields(m);

            deleteButton.setVisible(true);
            UI.getCurrent().addWindow(this);
        } catch (Exception ex) {
            logger.error("Error while editing a processController. ", ex);
            Notification.show("Error while editing: " + ex.getMessage(), Notification.Type.ERROR_MESSAGE);
        }
    }

    /**
     * When this method is called, a create window will be created for creating a aircraft.
     *
     */
    public void create() {
        setCaption("New ProcessController");

        //Bind the field to a brand new aircraft that is still empty.
        bindingFields(new ProcessController());

        UI.getCurrent().addWindow(this);
    }

    private void bindingFields(ProcessController m) {

        //Make the binder for the processController object
        binder = new BeanFieldGroup<>(ProcessController.class);

        //Set the dataSource
        binder.setItemDataSource(m);

        //The type of the processController
        ComboBox processControllerComboBox = new ComboBox("Type");

//      processControllerComboBox.setInputPrompt("e.g. Bomber");
        binder.bind(processControllerComboBox, "processControllerType");
        processControllerComboBox.setWidth("200");

        //Create the items based on the enums values
        for (ProcessControllerType processControllerTypeValue : ProcessControllerType.values()) {
            processControllerComboBox.addItem(processControllerTypeValue);
//            processControllerComboBox.setItemCaption(processControllerTypeValue, processControllerTypeValue.toString());
        }


        layout.addComponent(processControllerComboBox);

        layout.addComponent(buttons);
    }


    @Override
    public void buttonClick(Button.ClickEvent event) {

        if (event.getButton() == saveButton) { //If the saveButton was clicked.

            try {
                //Commit the binder to check if the validation passes.
                binder.commit();

            } catch (FieldGroup.CommitException e) {
                Notification.show("Error while commiting.");
                return;
            }

            try {
                datasource.addEntity(binder.getItemDataSource().getBean());
                Notification.show("ProcessController persisted!", Notification.Type.HUMANIZED_MESSAGE);

            } catch (Exception e) {
                logger.debug("Exception while persisting a processController!", e);
                Notification.show("Exception while persisting a processController!\n" + e
                        .getMessage(), Notification.Type.ERROR_MESSAGE);
                return;
            }
        } else if (event.getButton() == deleteButton) { //If the deleteButton was clicked.

            //Create a dialog to confirm this big step.
            ConfirmDialog.show(this.getUI(), "Are you sure you want to delete this processController?",
                    new ConfirmDialog.Listener() {
                        public void onClose(ConfirmDialog dialog) {
                            if (dialog.isConfirmed()) { //If we have a confirmation.
                                try {

                                    //Remove this item
                                    datasource.removeItem(binder.getItemDataSource().getBean().getId());

                                } catch (Exception e) {
                                    logger.debug("Exception while removing a processController!", e);
                                    Notification.show("Exception while removing a processController! \n" + e
                                            .getMessage(), Notification.Type.ERROR_MESSAGE);
                                }
                                close();
                            }
                        }
                    });
            return;
        } else if (event.getButton() == cancelButton) { //If the cancel button was clicked.

            binder.discard(); //Discard everything from this form.
        }
        close();
    }
}
