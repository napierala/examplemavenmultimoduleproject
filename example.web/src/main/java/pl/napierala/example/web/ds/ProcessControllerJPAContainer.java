package pl.napierala.example.web.ds;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.addon.jpacontainer.provider.CachingMutableLocalEntityProvider;
import pl.napierala.example.datasource.model.ProcessController;

import javax.persistence.EntityManager;
import java.util.Collection;

public class ProcessControllerJPAContainer extends JPAContainer<ProcessController> {


    /**
     * The persistance UNIT(from persistance.xml).
     */
    private static final String PERSISTENCE_UNIT = "ExampleUnit";

    public ProcessControllerJPAContainer() {
        super(ProcessController.class);
        EntityManager em = JPAContainerFactory.createEntityManagerForPersistenceUnit(PERSISTENCE_UNIT);
        setEntityProvider(new CachingMutableLocalEntityProvider<>(ProcessController.class, em));
    }

    @Override
    public Collection<Filter> getContainerFilters() {
        return null;
    }
}
