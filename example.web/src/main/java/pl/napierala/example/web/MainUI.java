package pl.napierala.example.web;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.server.DefaultErrorHandler;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.napierala.example.web.ds.ProcessControllerJPAContainer;
import pl.napierala.example.web.ui.ProcessControllerTable;
import pl.napierala.example.web.ui.ProcessControllerWindow;

import javax.servlet.annotation.WebServlet;


@Theme("whiteTheme")
@Widgetset("pl.napierala.example.web.MyAppWidgetset")
public class MainUI extends UI {

    static final Logger logger = LoggerFactory.getLogger(MainUI.class);

    private Table table;

    @Override
    protected void init(VaadinRequest vaadinRequest) {

        //Use our error handler
        addErrorHandle();

    }

    @WebServlet(urlPatterns = "/*", name = "MainUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MainUI.class, productionMode = false)
    public static class MainUIServlet extends VaadinServlet {
    }

    /**
     * A error handler to show all of the errors in a custom way.
     *
     */
    private void addErrorHandle() {

        UI.getCurrent().setErrorHandler(new DefaultErrorHandler() {
            @Override
            public void error(com.vaadin.server.ErrorEvent event) {
                String cause = "Error:\n";
                for (Throwable t = event.getThrowable(); t != null;
                     t = t.getCause())
                    if (t.getCause() == null)
                        cause += t.getClass().getName() + "\n";

                logger.error("Error",event.getThrowable());
                Notification.show(cause, Notification.Type.ERROR_MESSAGE);

                doDefault(event);
            }
        });
    }
}
