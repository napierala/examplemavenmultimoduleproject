package pl.napierala.example.web;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.server.DefaultErrorHandler;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.napierala.example.web.ds.ProcessControllerJPAContainer;
import pl.napierala.example.web.ui.ProcessControllerTable;
import pl.napierala.example.web.ui.ProcessControllerWindow;


@Theme("whiteTheme")
@Widgetset("pl.napierala.example.web.MyAppWidgetset")
public class ProcessControllerUI extends UI {

    static final Logger logger = LoggerFactory.getLogger(ProcessControllerUI.class);

    private Table table;

    @Override
    protected void init(VaadinRequest vaadinRequest) {

        //Create the datasource
        final ProcessControllerJPAContainer datasource = new ProcessControllerJPAContainer();

        //This table will use the datasource
        table = new ProcessControllerTable(datasource);

        //When we click on a item the aircraft edit window appears from the ProcessControllerWindow class.
        table.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            @Override
            public void itemClick(ItemClickEvent event) {
                ProcessControllerWindow window = new ProcessControllerWindow(datasource);
                window.edit(Long.valueOf(event.getItemId().toString()));
            }
        });

        //This UI will have just one vertical layout
        VerticalLayout content = new VerticalLayout();
//      content.setHeight("100%");
        setContent(content);

        //The vertical layout will consist of the top, the table, the buttons and a bottom
        VerticalLayout vertical = putAllOnVertical(buildTop(), table,
                buildButtons(datasource), buildBottom());

        content.addComponent(vertical);
        content.setComponentAlignment(vertical, Alignment.MIDDLE_CENTER);

        //Use our error handler
        addErrorHandle();

    }

    @WebServlet(urlPatterns = "/processController/*", name = "ProcessControllerUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = ProcessControllerUI.class, productionMode = false)
    public static class ProcessControllerUIServlet extends VaadinServlet {
    }

    /**
     * Get a dynamic amount of components and put them all unto a vertical layout.
     *
     * @param components
     *          The dynamic amount of components.
     *
     * @return a VerticalLayout consisting of all of the components.
     */
    private VerticalLayout putAllOnVertical(Component... components) {
        VerticalLayout vertical = new VerticalLayout();
        for (Component c: components) {
            if (c != null) {
                vertical.addComponent(c);
                vertical.setComponentAlignment(c, Alignment.MIDDLE_CENTER);
            }
        }
        return vertical;
    }

    /**
     * Build the top of the webPage(a Title and stuff like that).
     *
     * @return A HorizontalLayout that defines the top.
     */
    private HorizontalLayout buildTop() {

        //Simple title label
        Label title = new Label("Process Controllers");

        HorizontalLayout layoutTop = new HorizontalLayout();
        layoutTop.setHeight("100");
        layoutTop.setWidth("650");

        layoutTop.addComponent(title);
        layoutTop.setExpandRatio(title, 1.0f);

        layoutTop.setComponentAlignment(title, Alignment.MIDDLE_CENTER);

        return layoutTop;
    }

    /**
     * Build the buttons part of the web page(a new and update buttons).
     *
     * @param datasource
     *          A previously created datasource that we will use.
     *
     * @return A HorizontalLayout that defines the buttons.
     */
    private HorizontalLayout buildButtons(final ProcessControllerJPAContainer datasource) {

        //New button
        Button newButton = new Button("New");

        //When we click on the new button the processController create window appears from the ProcessControllerWindow class.
        newButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                ProcessControllerWindow window = new ProcessControllerWindow(datasource);
                window.create();
            }
        });

        //Update button
        Button updateButton = new Button("Update");

        //Refresh the datasource when the update button is clicked.
        updateButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                datasource.refresh();
            }
        });

        Button[] buttons = {newButton, updateButton};
        HorizontalLayout barButton = new HorizontalLayout();
        barButton.setHeight("50");

        for (Button b: buttons) {
            barButton.addComponent(b);
            barButton.setComponentAlignment(b, Alignment.MIDDLE_CENTER);
        }

        return barButton;
    }

    /**
     * Build the bottom of the webPage.
     *
     * @return A HorizontalLayout that defines the bottom.
     */
    private HorizontalLayout buildBottom() {

        //Just a test label
        String content = "Super Duper System";
        Label text = new Label(content);
        text.setContentMode(ContentMode.HTML);

        HorizontalLayout layoutbottom = new HorizontalLayout();
        layoutbottom.setHeight("100");
        layoutbottom.setWidth("650");
        layoutbottom.addComponent(text);

        return layoutbottom;
    }

    /**
     * A error handler to show all of the errors in a custom way.
     *
     */
    private void addErrorHandle() {

        UI.getCurrent().setErrorHandler(new DefaultErrorHandler() {
            @Override
            public void error(com.vaadin.server.ErrorEvent event) {
                String cause = "Error:\n";
                for (Throwable t = event.getThrowable(); t != null;
                     t = t.getCause())
                    if (t.getCause() == null)
                        cause += t.getClass().getName() + "\n";

                logger.error("Error",event.getThrowable());
                Notification.show(cause, Notification.Type.ERROR_MESSAGE);

                doDefault(event);
            }
        });
    }
}
