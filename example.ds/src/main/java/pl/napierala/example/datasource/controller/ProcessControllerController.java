package pl.napierala.example.datasource.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.napierala.example.datasource.exception.ResourceNotFoundException;
import pl.napierala.example.datasource.model.ProcessController;
import pl.napierala.example.datasource.repository.ProcessControllerRepository;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/processController")
public class ProcessControllerController {

    @Autowired
    private ProcessControllerRepository processControllerRepository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    public List<ProcessController> getProcessControllerList() {

        return processControllerRepository.findAll();
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ProcessController getProcessControllerById(@PathVariable Long id) {

        ProcessController item = processControllerRepository.findOne(id);

        if (item == null) {
            throw new ResourceNotFoundException(id);
        }

        return item;

    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ProcessController createProcessController(@RequestBody @Valid final ProcessController processController) {

        return processControllerRepository.save(processController);
    }


    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    public ProcessController updateProcessController(@RequestBody @Valid final ProcessController processController) {

        return processControllerRepository.save(processController);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteProcessControllerById(@PathVariable Long id) {

        if (!processControllerRepository.exists(id)) {
            throw new ResourceNotFoundException(id);
        }

        processControllerRepository.delete(id);

    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteProcessController(@RequestBody @Valid final ProcessController processController) {

        if (!processControllerRepository.exists(processController.getId())) {
            throw new ResourceNotFoundException(processController.getId());
        }

        processControllerRepository.delete(processController);
    }
}
