package pl.napierala.example.datasource.model;

/**
 * The Type of the controller type.
 */
public enum ProcessControllerType {
    LIGHT, POWER_SOCKET
}
