package pl.napierala.example.datasource.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.napierala.example.datasource.model.ProcessController;

import javax.persistence.Table;

@Repository
@Table(name = "ProcessController")
public interface ProcessControllerRepository extends JpaRepository<ProcessController, Long> {
}