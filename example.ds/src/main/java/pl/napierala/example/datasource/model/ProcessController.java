package pl.napierala.example.datasource.model;

import javax.persistence.*;

/**
 * The model of a ProcessController, an example entity.
 */
@Entity
@Table(name = "ProcessController")
public class ProcessController {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Long id = new Long(-1);

    @Column(name = "processControllerType")
    @Enumerated(EnumType.STRING)
    private ProcessControllerType processControllerType = null;

    public ProcessController() {

    }

    public ProcessController(Long id, ProcessControllerType processControllerType) {
        this.id = id;
        this.processControllerType = processControllerType;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProcessControllerType getProcessControllerType() {
        return processControllerType;
    }

    public void setProcessControllerType(ProcessControllerType processControllerType) {
        this.processControllerType = processControllerType;
    }

    @Override
    public String toString() {
        return "ProcessController{" +
                "id=" + id +
                ", processControllerType=" + processControllerType +
                '}';
    }
}
